#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
/*connection*/
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <netdb.h>
#include <netinet/in.h>

#define PROTOCOL_VERSION	20070		//プロトコルヴァージョンを表す整数
#define DEFAULT_SERVER		"192.168.11.5"	//デフォルトのサーバのアドレス 文字列で指定
#define DEFAULT_PORT		20000		//デフォルトのポート番号 整数で指定
#define DEFAULT_NAME		"default"	//デフォルトのクライアント名 文字列で指定

void entryToGame(void);
int closeSocket();
static int sendTable(char profile[128]);
static int openSocket(const char ip_addr[], uint16_t portnum_data);
static int sendProfile(const char user[15]);

#define SIZE 20
#define OUTER 3
#define KOMA 36
#define KOMA_SIDE 6
#define MOVEMODE 1         // 0:manual 1:auto
#define LOOP 10000
#define PRINTBOARD 0       // 0:none 1:show board after moved
#define PRINTINFO 0        // 0:none 1:show all infomation
#define C 1.4142

typedef struct {
  int masu[SIZE+OUTER+OUTER][SIZE+OUTER+OUTER]; // -1:outer 0:none 1:Player1 2:Player2
  int komaNum[2];           // 0:Player1 1:Player2
  int turnPlayer;           // 1:Player1 2:Player2
} Board;

typedef struct {
  int team;     // 1:player1 2:player2
  int alive;    // 0:dead 1:alive
  int health;   // =2
  int attack;   // =1
  int walkDis;  // =3
  int movable;  // 0:moved 1:can
  int stayable; // 0:can'tSty 1:canSty
  int place[2]; // 0:x 1:y
} Fu;

Board board;
Fu fu[KOMA+KOMA];

void iniBoard();
void iniFu();
void printBoard(int x,int y,int canMove[SIZE+OUTER+OUTER][SIZE+OUTER+OUTER],int mode);
int printHealth(int x,int y);
void changeTurn();
void moveFu(int monteMode);
int createMove(int inum,int tP,int x,int y,int canMove[SIZE+OUTER+OUTER][SIZE+OUTER+OUTER],int moveth, int moveNum);
void doneMove(int inum,int canMove[SIZE+OUTER+OUTER][SIZE+OUTER+OUTER],int dir);
int createAttack(int tP,int x,int y,int canMove[SIZE+OUTER+OUTER][SIZE+OUTER+OUTER]);
void doneAttack(int canMove[SIZE+OUTER+OUTER][SIZE+OUTER+OUTER],int dir);
void attacked(int x,int y,int damage);


int ai_Umeneri(int playerID,int canMove[SIZE+OUTER+OUTER][SIZE+OUTER+OUTER],int moveNum,int mode);
int monteMove(int canMove[SIZE+OUTER+OUTER][SIZE+OUTER+OUTER],int moveNum,int tP, int nowX, int nowY);
int playout(int tP,int toX,int toY,int nowX,int nowY);


long turnCount;
int switchMM;

int main(void)
{

  srand((unsigned)time(NULL));

  long turnNum,total,total2nd;
  double turnAve;
  long i;
  long first,second;

  total = 0;
  total2nd = 0;
  first = 0;
  second = 0;
  switchMM = 0;

  /*
  entryToGame();
  closeSocket();
  exit(1);
  */  

  for (i=0;i<LOOP;i++) {
    turnNum=0;
    turnCount=0;
    iniBoard();
    iniFu();
    
    while (1) {
      changeTurn();
      turnNum++;
      turnCount++;
      
      moveFu(0);
      
      if (board.komaNum[0]==0 || board.komaNum[1]==0) break;
    }

    printf("i=%ld\n",i);
    printf("win = %d\n",board.turnPlayer);
    if      (board.turnPlayer==1) first++;
    else if (board.turnPlayer==2) second++;
    total += turnNum;
    printf("firstR: %0.2f%%  secondR: %0.2f%% \n",(double)first/(i+1)*100,(double)second/(i+1)*100);
    turnAve = (double)total/(i+1);
    printf("turnNumAve =     %6.2f\n",turnAve);
    total2nd += turnNum * turnNum;
    printf("variance   =   %8.2f\n",(double)total2nd/(i+1) - (double)turnAve*turnAve);
  }


  printf("\n\n");
  
  return 0;
}

void iniBoard()
{
  int i,j;
  for (i=0;i<SIZE+OUTER*2;i++) {
    for (j=0;j<SIZE+OUTER*2;j++) {
      if (i<OUTER || i>SIZE+OUTER-1 || j<OUTER || j>SIZE+OUTER-1) {
	board.masu[i][j] = -1;
      } else if (i<OUTER+KOMA_SIDE && j>SIZE+OUTER-KOMA_SIDE-1) {
	board.masu[i][j] = 2;
      } else if (i>SIZE+OUTER-KOMA_SIDE-1 && j<OUTER+KOMA_SIDE) {
	board.masu[i][j] = 1;
      } else {
	board.masu[i][j] = 0;
      }
    }
  }

  board.komaNum[0] = KOMA;
  board.komaNum[1] = KOMA;
  board.turnPlayer = 0;
  
  return;
}

void iniFu()
{
  int team;
  int i,j,num;
  
  for (i=0;i<KOMA+KOMA;i++) {
    fu[i].team = i/KOMA + 1;
    fu[i].alive = 1;
    fu[i].health = 2;
    fu[i].attack = 1;
    fu[i].movable = 0;
    fu[i].walkDis = 3;
    fu[i].stayable = 1;
  }

  num = 0;
  for (i=SIZE+OUTER-KOMA_SIDE;i<SIZE+OUTER;i++) {
    for (j=OUTER;j<OUTER+KOMA_SIDE;j++) {
      fu[num].place[0] = i;
      fu[num].place[1] = j;
      num++;
    }
  }
  for (i=OUTER;i<OUTER+KOMA_SIDE;i++) {
    for (j=SIZE+OUTER-KOMA_SIDE;j<SIZE+OUTER;j++) {
      fu[num].place[0] = i;
      fu[num].place[1] = j;
      num++;
    }
  }
  
  return;
}

void printBoard(int x,int y,int canMove[SIZE+OUTER+OUTER][SIZE+OUTER+OUTER],int mode)
{
  int i,j;
  printf("==============================================================\n");
  for (i=OUTER;i<SIZE+OUTER;i++) {
    for (j=OUTER;j<SIZE+OUTER;j++) {
      if (i==x && j==y) {
	if (board.masu[i][j]==1) {
	  printf("  =%d#=",printHealth(i,j));
	} else if (board.masu[i][j]==2) {
	  printf("  =%d&=",printHealth(i,j));
	} else {
	  printf("  [  ]");
	}
      } else if (canMove[i][j]!=0) {
	if      (mode==0) printf("  *%2d]",canMove[i][j]);
	else if (mode==1) {
	  printf(">%d",canMove[i][j]);
	  if      (board.masu[i][j]==1) printf("[%d#]",printHealth(i,j));
	  else if (board.masu[i][j]==2) printf("[%d&]",printHealth(i,j));
	}
      } else {
	if (board.masu[i][j]==0) {
	  printf("  [  ]");
	} else if (board.masu[i][j]==1) {
	  printf("  [%d#]",printHealth(i,j));
	} else if (board.masu[i][j]==2) {
	  printf("  [%d&]",printHealth(i,j));
	} else {
	  printf("   %2d ",board.masu[i][j]);
	}
      }
    }
    printf("\n");
  }
  printf("==============================================================\n");
}

int printHealth(int x,int y)
{
  int i;
  for (i=0;i<KOMA+KOMA;i++) {
    if (fu[i].place[0]==x && fu[i].place[1]==y) {
      return fu[i].health;
    }
  }
  
  return -9;
}

void changeTurn()
{
  int i;
  board.turnPlayer += 1;
  if (board.turnPlayer>2) board.turnPlayer=1;
  

  for (i=KOMA*(board.turnPlayer-1);i<KOMA*board.turnPlayer;i++) {
    if (fu[i].alive==1) {
      fu[i].movable = 1;
    }
  }

  if (PRINTINFO==1) printf("=== changed ===\n");
  return;
}

void moveFu(int monteMode)
{
  int i,j,k;
  int dir;
  int ali=board.komaNum[board.turnPlayer-1];
  int canMove[SIZE+OUTER+OUTER][SIZE+OUTER+OUTER];
  int dummyMove[SIZE+OUTER+OUTER][SIZE+OUTER+OUTER]={};
  int moveNum;
  int attackNum;
  int flag;

  while (1) {
    for (i=KOMA*(board.turnPlayer-1);i<KOMA*board.turnPlayer;i++) {
      if (fu[i].movable == 0) continue;

      for (j=0;j<SIZE+OUTER+OUTER;j++) {
	for (k=0;k<SIZE+OUTER+OUTER;k++) {
	  canMove[j][k] = 0;
	}
      }
      
      if (PRINTBOARD==1 && monteMode==0) printBoard(fu[i].place[0],fu[i].place[1],dummyMove,0);
      moveNum = createMove(i,board.turnPlayer,fu[i].place[0],fu[i].place[1],canMove,0,1);
      moveNum--;
      if (PRINTBOARD==1 && monteMode==0) printBoard(fu[i].place[0],fu[i].place[1],canMove,0);
      
      
      if (PRINTINFO==1 && monteMode==0) printf("move?\n");
      if      (MOVEMODE==0) scanf("%d",&dir);
      else if (MOVEMODE==1) {
	dir= ai_Umeneri(i,canMove,moveNum,0);
	if (PRINTINFO==1 && monteMode==0)printf("select = %d\n",dir);
      }
      
      if (dir==0) { // stay
	if (fu[i].stayable==0) {
	  if (PRINTINFO==1 && monteMode==0) printf("no stay!\n");
	  continue;
	}
      } else { // move

	if (dir>moveNum || dir<0) {
	  if (PRINTINFO==1 && monteMode==0) printf("no move!\n");
	  continue;
	}
	
	board.masu[fu[i].place[0]][fu[i].place[1]] = 0;
	for (j=0,flag=0;j<SIZE+OUTER+OUTER;j++) {
	  for (k=0;k<SIZE+OUTER+OUTER;k++) {
	    if (canMove[j][k]==dir) {
	      flag=1;
	      break;
	    }
	  }
	  if (flag==1) break;
	}
	board.masu[j][k] = fu[i].team;
	doneMove(i,canMove,dir);
	
      }
      
      // attack

      if (PRINTBOARD==1 && monteMode==0) printBoard(fu[i].place[0],fu[i].place[1],dummyMove,0);
      
      for (j=0;j<SIZE+OUTER+OUTER;j++) {
	for (k=0;k<SIZE+OUTER+OUTER;k++) {
	  canMove[j][k] = 0;
	}
      }

      attackNum = createAttack(board.turnPlayer,fu[i].place[0],fu[i].place[1],canMove);
      if (PRINTBOARD==1 && monteMode==0) printBoard(fu[i].place[0],fu[i].place[1],canMove,1);
      if (PRINTINFO==1 && monteMode==0) printf("attack?\n");
      if      (MOVEMODE==0) scanf("%d",&dir);
      else if (MOVEMODE==1) {
	dir = ai_Umeneri(i,canMove,attackNum,1);
	if (PRINTINFO==1 && monteMode==0) printf("select = %d\n",dir);
      }
	
      if (dir==0) {
	
      } else {
	doneAttack(canMove,dir);
      }
      
      fu[i].movable = 0;
      ali -= 1;
      
    }
    if (ali<1) break;
  }
  
  return;
}

int createMove(int inum,int tP,int x,int y,int canMove[SIZE+OUTER+OUTER][SIZE+OUTER+OUTER],int moveth,int moveNum)
{
  int i;
  
  if (moveth>fu[inum].walkDis-1) return moveNum;
  if (canMove[x][y]==0 && board.masu[x][y]==0) {
    canMove[x][y]=moveNum;
    moveNum++;
  }

  if (board.masu[x+1][y]==0 || board.masu[x+1][y]==tP) {
    if (canMove[x+1][y]==0 && board.masu[x+1][y]==0) {
      canMove[x+1][y] = moveNum;
      moveNum++;
    }
    moveNum=createMove(inum,tP,x+1,y,canMove,moveth+1,moveNum);
  }
  if (board.masu[x][y-1]==0 || board.masu[x][y-1]==tP) {
    if (canMove[x][y-1]==0 && board.masu[x][y-1]==0) {
      canMove[x][y-1] = moveNum;
      moveNum++;
    }
    moveNum=createMove(inum,tP,x,y-1,canMove,moveth+1,moveNum);
  }
  if (board.masu[x][y+1]==0 || board.masu[x][y+1]==tP) {
    if (canMove[x][y+1]==0 && board.masu[x][y+1]==0) {
      canMove[x][y+1] = moveNum;
      moveNum++;
    }
    moveNum=createMove(inum,tP,x,y+1,canMove,moveth+1,moveNum);
  }
  if (board.masu[x-1][y]==0 || board.masu[x-1][y]==tP) {
    if (canMove[x-1][y]==0 && board.masu[x-1][y]==0) {
      canMove[x-1][y] = moveNum;
      moveNum++;
    }
    moveNum=createMove(inum,tP,x-1,y,canMove,moveth+1,moveNum);
  }
  
  
  return moveNum;
}

void doneMove(int inum,int canMove[SIZE+OUTER+OUTER][SIZE+OUTER+OUTER],int dir)
{
  int i,j;
  for (i=OUTER;i<SIZE+OUTER;i++) {
    for (j=OUTER;j<SIZE+OUTER;j++) {
      if (canMove[i][j]==dir) {
	fu[inum].place[0] = i;
	fu[inum].place[1] = j;
	return;
      }
    }
  }
  
  return;
}

int createAttack(int tP,int x,int y,int canMove[SIZE+OUTER+OUTER][SIZE+OUTER+OUTER])
{
  int i=1;
  if (board.masu[x+1][y]==3-tP) {
    canMove[x+1][y] = i;
    i++;
  }
  if (board.masu[x][y-1]==3-tP) {
    canMove[x][y-1] = i;
    i++;
  }
  if (board.masu[x][y+1]==3-tP) {
    canMove[x][y+1] = i;
    i++;
  }
  if (board.masu[x-1][y]==3-tP) {
    canMove[x-1][y] = i;
    i++;
  }
  return i-1;
}

void doneAttack(int canMove[SIZE+OUTER+OUTER][SIZE+OUTER+OUTER],int dir)
{
  int i,j;
  for (i=OUTER;i<SIZE+OUTER;i++) {
    for (j=OUTER;j<SIZE+OUTER;j++) {
      if (canMove[i][j]==dir) {
	attacked(i,j,fu[i].attack);
	return;
      }
    }
  }
  
  return;
}

void attacked(int x,int y,int damage)
{
  int i;
  for (i=0;i<KOMA+KOMA;i++) {
    if (fu[i].place[0]==x && fu[i].place[1]==y) {
      fu[i].health -= 1;
      if (PRINTINFO==1) printf("health = %d\n",fu[i].health);
      break;
    }
  }

  if (fu[i].health < 1) {
    board.masu[x][y] = 0;
    board.komaNum[fu[i].team-1] -= 1;
    fu[i].alive = 0;
    fu[i].place[0] = 0;
    fu[i].place[1] = 0;
  }
  
  return;
}

int ai_Umeneri(int playerID,int canMove[SIZE+OUTER+OUTER][SIZE+OUTER+OUTER],int moveNum,int mode)
{
  int tP = fu[playerID].team;
  if ((turnCount+1)%100>50) tP = 3-tP; 
  int nowX = fu[playerID].place[0];
  int nowY = fu[playerID].place[1];

  int i;
  

  if (mode==0) {         // move

    //if (switchMM==0) return monteMove(canMove,moveNum,tP,nowX,nowY);
    
    // ランダムで動くやつ
    i = rand()%(moveNum+1);
    return i;
    
    
    // 斜めに動くやつ
    /*
      if (canMove[nowX+(2*tP-3)][nowY+(-2*tP+3)]>0) return canMove[nowX+(2*tP-3)][nowY+(-2*tP+3)];
    */

    
    
    
    
  } else if (mode==1) {  // attack
    if (moveNum>0) return 1;
  }


  return 0;
}

int monteMove(int canMove[SIZE+OUTER+OUTER][SIZE+OUTER+OUTER],int moveNum,int tP, int nowX, int nowY)
{
  switchMM = 1;
  long poLoop;
  double winRate[32]={};
  long poCount[32]={};
  double ucb;
  double maxucb;
  int select;
  int winPoint;
  int i;
  double s;

  int toX,toY,x,y;

  for (poLoop=0;poLoop<5;poLoop++) {
    printf("loop = %ld\n",poLoop);
    maxucb = -1;
    ucb = -1;
    select = -1;
    while (1) {
      for (i=0;i<moveNum;i++) {
	if (poCount[i]==0) ucb = rand()%25250 + 25250;
	else               ucb = winRate[i] + C * sqrt(log(poLoop) / poCount[i]);
	if (maxucb<ucb) {
	  select = i;
	  maxucb = ucb;
	}
      }
      
      if (select==-1) {
	printf("error: no select\n");
	continue;
      }
      break;
    }

    toX=-1;
    toY=-1;
    for (x=OUTER;x<SIZE+OUTER;x++) {
      for (y=OUTER;y<SIZE+OUTER;y++) {
	if (canMove[x][y]==select) {
	  toX = x;
	  toY = y;
	  break;
	}
      }
      if (toX!=-1 || toY!=-1) break;
    }

    winPoint = playout(tP,toX,toY,nowX,nowY);
    
    winRate[select] = (winRate[select] * poCount[select] + winPoint) / (poCount[select] + 1);
    poCount[select] += 1;
  }

  long maxCount=0;
  select = -1;
  
  for (i=0;i<moveNum;i++) {
    if (maxCount<poCount[i]) {
      select = i;
      maxCount = poCount[i];
    }
  }

  switchMM = 0;
  if (select!=-1) return select;
  return 0;
}

int playout(int tP,int toX,int toY,int nowX,int nowY)
{
  Board sub_board;
  Fu sub_fu[KOMA+KOMA];
  int i,j,k;

  int canMove[SIZE+OUTER+OUTER][SIZE+OUTER+OUTER];
  int attackNum;
  
  for (i=0;i<KOMA+KOMA;i++) if (fu[i].place[0]==nowX && fu[i].place[1]==nowY) break;
  if (i==KOMA+KOMA) return 0;
  
  sub_board = board;
  for (i=0;i<KOMA+KOMA;i++) sub_fu[i] = fu[i];

  for (i=0;i<SIZE+OUTER+OUTER;i++) {
    for (j=0;j<SIZE+OUTER+OUTER;j++) {
      for (k=0;k<KOMA+KOMA;k++) {
	if (board.masu[fu[k].place[0]][fu[k].place[1]] == 0) exit(1);
      }
    }
  }
  
  
  board.masu[nowX][nowY] = 0;
  board.masu[toX][toY] = tP;
  fu[i].place[0] = toX;
  fu[i].place[1] = toY;
  fu[i].movable = 0;

  for (i=0;i<SIZE+OUTER+OUTER;i++) {
    for (j=0;j<SIZE+OUTER+OUTER;j++) {
      canMove[i][j] = 0;
    }
  }
  attackNum = createAttack(tP,toX,toY,canMove);

  if (attackNum>0) doneAttack(canMove,1);

  while (1) {
    moveFu(1);
    
    if (board.komaNum[0]==0 || board.komaNum[1]==0) break;
    changeTurn();
  }
  
  if (board.komaNum[tP-1]==0) k=1;
  else                        k=0;

  board = sub_board;
  for (i=0;i<KOMA+KOMA;i++) fu[i] = sub_fu[i];
  return k;
}




/*connection内大域変数*/

//ソケット関連の変数を静的グローバル変数として宣言
static int g_sockfd;
static int g_buf_len;
static struct sockaddr_in g_client_addr;

//接続するサーバ、ポートを格納する変数
static char     server_name[256]= DEFAULT_SERVER;
static uint16_t port            = DEFAULT_PORT;
//サーバに通知するクライアント名
static char     user_name[15]   = DEFAULT_NAME;

//テーブルを受信した回数をカウント
static int table_count=0;

//hostに接続し ゲームに参加する プレーヤー番号を返す
void entryToGame(void){
  char my_playernum[16]="";  //プレイヤー番号を記憶する
  char banmen[1024]={};
  int i,j;
  //サーバに対してソケットを用意し、connectする
  if((openSocket(server_name,port))== -1){
    printf("failed to open socket to server[%s:%d].\n",server_name,port);
    exit (1);
  }

  sendProfile(user_name);     //クライアントの情報を送信


  printf("--- %s\n",my_playernum);
  //自身のプレイヤー番号をサーバからもらう
  
  if(read(g_sockfd, &my_playernum, sizeof(my_playernum)) > 0){
    //my_playernum=ntohl(my_playernum);
  }
  else{
    printf("failed to get player number.\n");
    exit (1);
  }
  printf("--- %s\n",my_playernum);
  
  if(read(g_sockfd, &my_playernum, sizeof(my_playernum)) > 0){
    //my_playernum=ntohl(my_playernum);
  }
  else{
    printf("failed to get player number.\n");
    exit (1);
  }
  printf("--- %s\n",my_playernum);
  if(read(g_sockfd, &my_playernum, sizeof(my_playernum)) > 0){
    //my_playernum=ntohl(my_playernum);
  }
  else{
    printf("failed to get player number.\n");
    exit (1);
  }
  printf("--- %s\n",my_playernum);

  
  if(read(g_sockfd, &banmen, sizeof(banmen)) > 0){
    //my_playernum=ntohl(my_playernum);
  }
  else{
    printf("failed to get player number.\n");
    exit (1);
  }
  printf("--- %s\n",banmen);
  
  return;
}


//ソケットのcloseを行う 成功した場合は 0 を返す。エラーが発生した場合は -1 を返す
int closeSocket(){
  return close(g_sockfd);
}

//サーバにテーブル情報を投げる関数。成功なら0　失敗時-1を返す
static int sendTable(char profile[16]){
  /*
  uint32_t net_table[8][15];
  int i,j;
  //全てのテーブルの要素をホストオーダーからネットワークオーダーに変換
  for(i=0;i<8;i++)
    for(j=0;j<15;j++)
      net_table[i][j]=htonl(table_val[i][j]);
  //変換したテーブルを送信
  if((g_buf_len = write(g_sockfd, net_table, 480))> 0){
    return (0);
  }
  else{
    return (-1);
  }
  */

  uint32_t net_char[16];
  long i;
  for (i=0;i<16;i++) net_char[i] = profile[i];
  //変換したテーブルを送信
  if((g_buf_len = write(g_sockfd, net_char, 20))> 0){
    return (0);
  }
  else{
    return (-1);
  }
  
  
}

//ソケットの設定・接続を行う 成功時0、失敗時-1を返す
static int openSocket(const char addr[], uint16_t port_num){
  //ソケットの生成
  if ((g_sockfd = socket(PF_INET, SOCK_STREAM, 0)) < 0){
    return(-1);
  }

  /* ポートとアドレスの設定 */
  bzero((char*)&g_client_addr,sizeof(g_client_addr));
  g_client_addr.sin_family = PF_INET;
  g_client_addr.sin_port = htons(port_num);
  g_client_addr.sin_addr.s_addr = inet_addr(addr);

  //IPアドレスで指定されていないとき、ホスト名の解決を試みる
  if (g_client_addr.sin_addr.s_addr == 0xffffffff) {
    struct hostent *host;
    host = gethostbyname(addr);
    if (host == NULL) {
      printf("failed to gethostbyname() : %s.\n",addr);
      return -1;//ホスト名解決に失敗したとき、-1を返す
    }
    g_client_addr.sin_addr.s_addr=
      *(unsigned int *)host->h_addr_list[0];
  }

  /* サーバにコネクトする */
  if (connect(g_sockfd,(struct sockaddr *)&g_client_addr, sizeof(g_client_addr)) == 0){
    return 0;
  }
  return -1;
}

//クライアントの情報を送信
static int sendProfile(const char user[15]){
  char profile[16]="tsumutsumu";
  int i;

  
  //送信
  if(sendTable(profile)==-1){                       //失敗したらエラーを出力し停止
    printf("sending profile table was failed.\n");
    exit (1);
  }

  return 0;
}
