#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
/*connection*/
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <netinet/in.h>

#define Recv_Size 20*3*20
#define Send_Size 3*3*36+3
#define PORTNUM 20001

int entryToGame();
void recvMapF();
void sendMsgF();
void createSend();
void changeFunction();
void makeBoard();

#define SIZE 20
#define OUTER 3
#define KOMA 36
#define KOMA_SIDE 6
#define MOVEMODE 1         // 0:manual 1:auto3
#define LOOP 10
#define PRINTBOARD 1       // 0:none 1:show board after moved
#define PRINTINFO 0        // 0:none 1:show all infomation
#define C 1.4142
#define LOCALF 0           // local battle
#define NETF 1             // net battle

int sd;                    //ソケット作成用の変数
struct sockaddr_in addr;   //サーバ接続用の変数
char *recv_Map;            //受信データ格納用の変数
char *send_Msg;            //送信データ格納用の変数

typedef struct {
  int masu[SIZE+OUTER+OUTER][SIZE+OUTER+OUTER]; // -1:outer 0:none 1:Player1 2:Player2
  int komaNum[2];          // 0:Player1 1:Player2
  int turnPlayer;          // 1:Player1 2:Player2
} Board;

typedef struct {
  int team;     // 1:player1 2:player2
  int alive;    // 0:dead 1:alive
  int health;   // =2
  int attack;   // =1
  int walkDis;  // =3
  int movable;  // 0:moved 1:can
  int stayable; // 0:can'tSty 1:canSty
  int place[2]; // 0:x 1:y
} Fu;

typedef struct {
  int oldX;
  int oldY;
  int newX;
  int newY;
  int attackX;
  int attackY;
} Move;

Board board;
Fu fu[KOMA+KOMA];
Move move[KOMA];
int moveID;

void iniBoard();
void iniFu();
int checkBoard();
void printBoard(int x,int y,int canMove[SIZE+OUTER+OUTER][SIZE+OUTER+OUTER],int mode);
void printFu();
int printHealth(int x,int y);
void changeTurn(int mode);

void moveFu(int monteMode);
int createMove(int inum,int tP,int x,int y,int canMove[SIZE+OUTER+OUTER][SIZE+OUTER+OUTER],int moveth, int moveNum);
void doneMove(int inum,int canMove[SIZE+OUTER+OUTER][SIZE+OUTER+OUTER],int dir);
int createAttack(int tP,int x,int y,int canMove[SIZE+OUTER+OUTER][SIZE+OUTER+OUTER]);
void doneAttack(int canMove[SIZE+OUTER+OUTER][SIZE+OUTER+OUTER],int dir,int moveID,int moveF);
void attacked(int x,int y,int damage);


int ai_Umeneri(int playerID,int canMove[SIZE+OUTER+OUTER][SIZE+OUTER+OUTER],int moveNum,int forceMove,int mode);
int monteMove(int canMove[SIZE+OUTER+OUTER][SIZE+OUTER+OUTER],int moveNum,int tP, int nowX, int nowY);
int playout(int tP,int toX,int toY,int nowX,int nowY);


long turnCount;
int switchMM;
int playiidd;

int main(void)
{

  srand((unsigned)time(NULL));

  long turnNum,total,total2nd;
  double turnAve;
  long i;
  long first,second;

  total = 0;
  total2nd = 0;
  first = 0;
  second = 0;
  switchMM = 0;
  //playiidd = 0;

  if (NETF==1) {
    entryToGame();
    printf("--- entry\n");
    
    turnNum=0;
    turnCount=0;
    

    while (1) {

      
      if (PORTNUM==20000) {
	makeBoard();    // 盤面生成
	printf("--- makeboard\n");
	moveFu(0);      // 行動
	printf("--- movefu\n");
	createSend();   // 送信データ作成
	printf("--- createsend\n");
	sendMsgF();     // 送信
	printf("--- sendmsgf\n");
	recvMapF();     // 受信 01
	printf("--- recvmapf 01\n");
	recvMapF();     // 受信 02
	printf("--- recvmapf 02\n");
	
      } else if (PORTNUM==20001) {
	recvMapF();     // 受信 01
	printf("--- recvmapf 01\n");
	makeBoard();    // 盤面生成
	printf("--- makeboard\n");
	moveFu(0);      // 行動
	printf("--- movefu\n");
	createSend();   // 送信データ作成
	printf("--- createsend\n");
	sendMsgF();     // 送信
	printf("--- sendmsgf\n");
	recvMapF();     // 受信 02
	printf("--- recvmapf 02\n");
	
      }

      changeFunction();
    }

    close(sd);
    
  }

  if (LOCALF==1) {
    for (i=0;i<LOOP;i++) {
      turnNum=0;
      turnCount=0;
      iniBoard();
      iniFu();
      
      while (1) {
	changeTurn(0);
	turnNum++;
	turnCount++;
	
	moveFu(0);
	
	if (board.komaNum[0]==0 || board.komaNum[1]==0) break;
      }
      
      printf("i=%ld\n",i);
      printf("win = %d\n",board.turnPlayer);
      if      (board.turnPlayer==1) first++;
      else if (board.turnPlayer==2) second++;
      total += turnNum;
      printf("firstR: %0.2f%%  secondR: %0.2f%% \n",(double)first/(i+1)*100,(double)second/(i+1)*100);
      turnAve = (double)total/(i+1);
      printf("turnNumAve =     %6.2f\n",turnAve);
      total2nd += turnNum * turnNum;
      printf("variance   =   %8.2f\n",(double)total2nd/(i+1) - (double)turnAve*turnAve);
    }
  }

  
  printf("\n\n");
  
  return 0;
}

void iniBoard()
{
  int i,j;
  for (i=0;i<SIZE+OUTER*2;i++) {
    for (j=0;j<SIZE+OUTER*2;j++) {
      if (i<OUTER || i>SIZE+OUTER-1 || j<OUTER || j>SIZE+OUTER-1) {
	board.masu[i][j] = -1;
      } else if (i<OUTER+KOMA_SIDE && j>SIZE+OUTER-KOMA_SIDE-1) {
	board.masu[i][j] = 2;
      } else if (i>SIZE+OUTER-KOMA_SIDE-1 && j<OUTER+KOMA_SIDE) {
	board.masu[i][j] = 1;
      } else {
	board.masu[i][j] = 0;
      }
    }
  }

  board.komaNum[0] = KOMA;
  board.komaNum[1] = KOMA;
  board.turnPlayer = 0;
  
  return;
}

void iniFu()
{
  int team;
  int i,j,num;
  
  for (i=0;i<KOMA+KOMA;i++) {
    fu[i].team = i/KOMA + 1;
    fu[i].alive = 1;
    fu[i].health = 2;
    fu[i].attack = 1;
    fu[i].movable = 0;
    fu[i].walkDis = 3;
    fu[i].stayable = 1;
  }

  num = 0;
  for (i=SIZE+OUTER-KOMA_SIDE;i<SIZE+OUTER;i++) {
    for (j=OUTER;j<OUTER+KOMA_SIDE;j++) {
      fu[num].place[0] = i;
      fu[num].place[1] = j;
      num++;
    }
  }
  for (i=OUTER;i<OUTER+KOMA_SIDE;i++) {
    for (j=SIZE+OUTER-KOMA_SIDE;j<SIZE+OUTER;j++) {
      fu[num].place[0] = i;
      fu[num].place[1] = j;
      num++;
    }
  }
  
  return;
}

int checkBoard()
{
  int i,j,count[2]={};

  
  for (i=0;i<SIZE+OUTER+OUTER;i++) {
    for (j=0;j<SIZE+OUTER+OUTER;j++) {
      if (i<OUTER || i>SIZE+OUTER-1 || j<OUTER || j>SIZE+OUTER-1) {
	if (board.masu[i][j]!=-1) {
	  printf("error: board.masu[%d][%d] = %d\n",i,j,board.masu[i][j]);
	  return 1;
	}
      } else {
	if (board.masu[i][j]<0) {
	  printf("error: board.masu[%d][%d] = %d\n",i,j,board.masu[i][j]);
	  return 1;
	}
      }

      if (board.masu[i][j]>0) {
	count[board.masu[i][j]-1] += 1;
      }
      
    }
  }

  if (count[0]!=board.komaNum[0]) {
    printf("error: count[0]=%d board.komaNum[0]=%d\n",count[0],board.komaNum[0]);
    return 1;
  }
  if (count[1]!=board.komaNum[1]) {
    printf("error: count[1]=%d board.komaNum[1]=%d\n",count[1],board.komaNum[1]);
    return 1;
  }

  for (i=0;i<KOMA+KOMA;i++) {
    if (fu[i].alive==1 && fu[i].health<1) {
      printf("error: fu[%d] is alive, but the health is [%d]\n",i,fu[i].health);
      return 1;
    }
    if (fu[i].alive==0 && fu[i].health>0) {
      printf("error: fu[%d] is dead, but the health is [%d]\n",i,fu[i].health);
      return 1;
    }
  }
  
  
  return 0;
}

void printBoard(int x,int y,int canMove[SIZE+OUTER+OUTER][SIZE+OUTER+OUTER],int mode)
{
  int i,j;
  printf("==============================================================\n");
  if (mode!=2) {
    for (i=OUTER;i<SIZE+OUTER;i++) {
      for (j=OUTER;j<SIZE+OUTER;j++) {
	if (i==x && j==y) {
	  if (board.masu[i][j]==1) {
	    printf("  =%d#=",printHealth(i,j));
	  } else if (board.masu[i][j]==2) {
	    printf("  =%d&=",printHealth(i,j));
	  } else {
	    printf("  [  ]");
	  }
	} else if (canMove[i][j]!=0) {
	  if      (mode==0) printf("  *%2d]",canMove[i][j]);
	  else if (mode==1) {
	    printf(">%d",canMove[i][j]);
	    if      (board.masu[i][j]==1) printf("[%d#]",printHealth(i,j));
	    else if (board.masu[i][j]==2) printf("[%d&]",printHealth(i,j));
	  }
	} else {
	  if (board.masu[i][j]==0) {
	    printf("  [  ]");
	  } else if (board.masu[i][j]==1) {
	    printf("  [%d#]",printHealth(i,j));
	  } else if (board.masu[i][j]==2) {
	    printf("  [%d&]",printHealth(i,j));
	  } else {
	    printf("   %2d ",board.masu[i][j]);
	  }
	}
      }
      printf("\n");
    }
  } else if (mode==2) {
    for (i=0;i<SIZE+OUTER+OUTER;i++) {
      for (j=0;j<SIZE+OUTER+OUTER;j++) {
	printf(" %2d",board.masu[i][j]);
      }
      printf("\n");
    }
  }
  printf("==============================================================\n");
  return;
}

void printFu()
{
  int i;
  printf("~~~~~~~~~~~~~~~~~~~~~~~\n\n");
  printf("  i tm al h mv plx ply\n");
  for (i=0;i<KOMA+KOMA;i++) {
    printf(" %02d  %d  %d %d  %d  %02d  %02d\n",i,fu[i].team,fu[i].alive,fu[i].health,fu[i].movable,fu[i].place[0],fu[i].place[1]);
  }
  printf("\n~~~~~~~~~~~~~~~~~~~~~~~\n");
  
  return;
}

int printHealth(int x,int y)
{
  int i;
  for (i=0;i<KOMA+KOMA;i++) {
    if (fu[i].place[0]==x && fu[i].place[1]==y) {
      return fu[i].health;
    }
  }
  
  return -810;
}

void changeTurn(int mode)
{
  int i;
  board.turnPlayer += 1;
  if (board.turnPlayer>2) board.turnPlayer=1;
  playiidd += 1;
  if (playiidd>2) playiidd=1;
  

  for (i=KOMA*(board.turnPlayer-1);i<KOMA*board.turnPlayer;i++) {
    if (fu[i].alive==1) {
      fu[i].movable = 1;
    }
  }

  if (PRINTINFO==1 && mode==0) printf("=== changed ===\n");
  return;
}

void moveFu(int monteMode)
{
  int i,j,k;
  int dir;
  int ali=board.komaNum[board.turnPlayer-1];
  int canMove[SIZE+OUTER+OUTER][SIZE+OUTER+OUTER];
  int dummyMove[SIZE+OUTER+OUTER][SIZE+OUTER+OUTER]={};
  int moveNum;
  int attackNum;
  int flag;

  long tester=0;
  int forceMove=0;

  moveID =0;
  int moveF=0;

  printf("---- ali = %d\n",ali);
  printFu();
  
  while (1) {
    if (checkBoard()) {
      printf("error: checkBoard()\n");
      printf("komaNum[0] = %d\n",board.komaNum[0]);
      printf("komaNum[1] = %d\n",board.komaNum[1]);
      printBoard(0,0,dummyMove,0);
      printBoard(0,0,dummyMove,2);
      printFu();
      exit(1);
    }

    printFu();
    
    //for (i=KOMA*(board.turnPlayer-1);i<KOMA*board.turnPlayer;i++) {
    for (i=0;i<KOMA+KOMA;i++) {
       
      if (monteMode==1) tester++;
      
      if (tester>1024) {
	ali -= 1;
	forceMove = 1;
      }
      
      if (fu[i].movable == 0) continue;
      
      for (j=0;j<SIZE+OUTER+OUTER;j++) {
	for (k=0;k<SIZE+OUTER+OUTER;k++) {
	  canMove[j][k] = 0;
	}
      }

      printf("---- begin move\n");
      
      if (PRINTBOARD==1 && monteMode==0) printBoard(fu[i].place[0],fu[i].place[1],dummyMove,0);
      moveNum = createMove(i,board.turnPlayer,fu[i].place[0],fu[i].place[1],canMove,0,1);
      moveNum--;
      if (PRINTBOARD==1 && monteMode==0) printBoard(fu[i].place[0],fu[i].place[1],canMove,0);

      printf("---- after createmove\n");
      
      if (PRINTINFO==1 && monteMode==0) printf("move?\n");
      if      (MOVEMODE==0) scanf("%d",&dir);
      else if (MOVEMODE==1) {
	dir= ai_Umeneri(i,canMove,moveNum,forceMove,0);
	if (PRINTINFO==1 && monteMode==0)printf("select = %d\n",dir);
      }

      printf("---- begin select\n");
      
      if (dir==0) { // stay
	if (fu[i].stayable==0) {
	  if (PRINTINFO==1 && monteMode==0) printf("no stay!\n");
	  continue;
	}
	
	if (NETF==1) {
	  move[moveID].oldX = fu[i].place[0];
	  move[moveID].oldY = fu[i].place[1];
	  move[moveID].newX = fu[i].place[0];
	  move[moveID].newY = fu[i].place[1];
	  moveF=1;
	}
	
      } else { // move

	if (dir>moveNum || dir<0) {
	  if (PRINTINFO==1 && monteMode==0) printf("no move!\n");
	  continue;
	}
	
	board.masu[fu[i].place[0]][fu[i].place[1]] = 0;
	if (NETF==1) {
	  move[moveID].oldX = fu[i].place[0];
	  move[moveID].oldY = fu[i].place[1];
	  moveF=1;
	}
	for (j=0,flag=0;j<SIZE+OUTER+OUTER;j++) {
	  for (k=0;k<SIZE+OUTER+OUTER;k++) {
	    if (canMove[j][k]==dir) {
	      flag=1;
	      break;
	    }
	  }
	  if (flag==1) break;
	}
	board.masu[j][k] = fu[i].team;
	if (moveF==1) {
	  move[moveID].newX = j;
	  move[moveID].newY = k;
	}
	doneMove(i,canMove,dir);
	
      }

      printf("---- begin attack\n");
      // attack

      if (PRINTBOARD==1 && monteMode==0) printBoard(fu[i].place[0],fu[i].place[1],dummyMove,0);
      
      for (j=0;j<SIZE+OUTER+OUTER;j++) {
	for (k=0;k<SIZE+OUTER+OUTER;k++) {
	  canMove[j][k] = 0;
	}
      }

      attackNum = createAttack(board.turnPlayer,fu[i].place[0],fu[i].place[1],canMove);
      if (PRINTBOARD==1 && monteMode==0) printBoard(fu[i].place[0],fu[i].place[1],canMove,1);
      if (PRINTINFO==1 && monteMode==0) printf("attack?\n");
      if      (MOVEMODE==0) scanf("%d",&dir);
      else if (MOVEMODE==1) {
	dir = ai_Umeneri(i,canMove,attackNum,0,1);
	if (PRINTINFO==1 && monteMode==0) printf("select = %d\n",dir);
      }
	
      if (dir==0 || dir>attackNum) {
	if (moveF==1) {
	  move[moveID].attackX = move[moveID].newX;
	  move[moveID].attackY = move[moveID].newY;
	}
      } else {
	doneAttack(canMove,dir,moveID,moveF);
      }
      
      if (NETF==1) {
	moveF = 0;
	moveID += 1;
      }
      
      fu[i].movable = 0;
      ali -= 1;
      
    }
    printf("---- ali?0\n");
    if (ali<1) break;
  }
  
  return;
}

int createMove(int inum,int tP,int x,int y,int canMove[SIZE+OUTER+OUTER][SIZE+OUTER+OUTER],int moveth,int moveNum)
{
  int i;
  
  if (moveth>fu[inum].walkDis-1) return moveNum;
  if (canMove[x][y]==0 && board.masu[x][y]==0) {
    canMove[x][y]=moveNum;
    moveNum++;
  }

  if (board.masu[x+1][y]==0 || board.masu[x+1][y]==tP) {
    if (canMove[x+1][y]==0 && board.masu[x+1][y]==0) {
      canMove[x+1][y] = moveNum;
      moveNum++;
    }
    moveNum=createMove(inum,tP,x+1,y,canMove,moveth+1,moveNum);
  }
  if (board.masu[x][y-1]==0 || board.masu[x][y-1]==tP) {
    if (canMove[x][y-1]==0 && board.masu[x][y-1]==0) {
      canMove[x][y-1] = moveNum;
      moveNum++;
    }
    moveNum=createMove(inum,tP,x,y-1,canMove,moveth+1,moveNum);
  }
  if (board.masu[x][y+1]==0 || board.masu[x][y+1]==tP) {
    if (canMove[x][y+1]==0 && board.masu[x][y+1]==0) {
      canMove[x][y+1] = moveNum;
      moveNum++;
    }
    moveNum=createMove(inum,tP,x,y+1,canMove,moveth+1,moveNum);
  }
  if (board.masu[x-1][y]==0 || board.masu[x-1][y]==tP) {
    if (canMove[x-1][y]==0 && board.masu[x-1][y]==0) {
      canMove[x-1][y] = moveNum;
      moveNum++;
    }
    moveNum=createMove(inum,tP,x-1,y,canMove,moveth+1,moveNum);
  }
  
  
  return moveNum;
}

void doneMove(int inum,int canMove[SIZE+OUTER+OUTER][SIZE+OUTER+OUTER],int dir)
{
  int i,j;
  for (i=OUTER;i<SIZE+OUTER;i++) {
    for (j=OUTER;j<SIZE+OUTER;j++) {
      if (canMove[i][j]==dir) {
	fu[inum].place[0] = i;
	fu[inum].place[1] = j;
	return;
      }
    }
  }
  
  return;
}

int createAttack(int tP,int x,int y,int canMove[SIZE+OUTER+OUTER][SIZE+OUTER+OUTER])
{
  int i=1;
  if (board.masu[x+1][y]==3-tP) {
    canMove[x+1][y] = i;
    i++;
  }
  if (board.masu[x][y-1]==3-tP) {
    canMove[x][y-1] = i;
    i++;
  }
  if (board.masu[x][y+1]==3-tP) {
    canMove[x][y+1] = i;
    i++;
  }
  if (board.masu[x-1][y]==3-tP) {
    canMove[x-1][y] = i;
    i++;
  }
  return i-1;
}

void doneAttack(int canMove[SIZE+OUTER+OUTER][SIZE+OUTER+OUTER],int dir,int moveID,int moveF)
{
  int i,j;
  for (i=OUTER;i<SIZE+OUTER;i++) {
    for (j=OUTER;j<SIZE+OUTER;j++) {
      if (canMove[i][j]==dir) {
	attacked(i,j,fu[i].attack);
	if (moveF==1) {
	  move[moveID].attackX = i;
	  move[moveID].attackY = j;
	}
	return;
      }
    }
  }
  
  return;
}

void attacked(int x,int y,int damage)
{
  int i;
  for (i=0;i<KOMA+KOMA;i++) {
    if (fu[i].place[0]==x && fu[i].place[1]==y) {
      fu[i].health -= 1;
      if (PRINTINFO==1 && switchMM==0) printf("health = %d\n",fu[i].health);
      break;
    }
  }

  if (fu[i].health < 1) {
    board.masu[x][y] = 0;
    board.komaNum[fu[i].team-1] -= 1;
    fu[i].alive = 0;
    fu[i].place[0] = 0;
    fu[i].place[1] = 0;
  }
  
  return;
}

int ai_Umeneri(int playerID,int canMove[SIZE+OUTER+OUTER][SIZE+OUTER+OUTER],int moveNum,int forceMove,int mode)
{
  int tP = fu[playerID].team;
  if ((turnCount+1)%100>50) tP = 3-tP; 
  int nowX = fu[playerID].place[0];
  int nowY = fu[playerID].place[1];

  int i;

  /*
  if (playiidd==2) {
    i = rand()%(moveNum+1);
    return i;
    
  }
  */

  if (mode==0) {         // move

    // if (forceMove==1)

    //if (switchMM==1 && moveNum==0) return 0;

    //if (switchMM==0) return monteMove(canMove,moveNum,tP,nowX,nowY);
    
    // ランダムで動くやつ
    i = rand()%(moveNum+1);
    return i;
    
    
    // 斜めに動くやつ
    /*
      if (canMove[nowX+(2*tP-3)][nowY+(-2*tP+3)]>0) return canMove[nowX+(2*tP-3)][nowY+(-2*tP+3)];
    */

    
    
    
    
  } else if (mode==1) {  // attack
    if (moveNum>0) return 1;
  }


  return 0;
}

int monteMove(int canMove[SIZE+OUTER+OUTER][SIZE+OUTER+OUTER],int moveNum,int tP, int nowX, int nowY)
{
  switchMM = 1;
  long poLoop;
  double winRate[32]={};
  long poCount[32]={};
  double ucb;
  double maxucb;
  int select;
  int winPoint;
  int i;
  double s;

  int toX,toY,x,y;

  if (moveNum==0) return 0;

  for (poLoop=0;poLoop<5;poLoop++) {
    //printf("loop = %ld\n",poLoop);
    while (1) {
      maxucb = -1;
      ucb = -1;
      select = -1;
      for (i=0;i<moveNum;i++) {
	if (poCount[i]==0) ucb = rand()%25250 + 25250;
	else               ucb = winRate[i] + C * sqrt(log(poLoop) / poCount[i]);
	if (maxucb<ucb) {
	  select = i;
	  maxucb = ucb;
	}
      }
      
      if (select==-1) {
	printf("error: no select\n");
	continue;
      }
      break;
    }

    toX=-1;
    toY=-1;
    for (x=OUTER;x<SIZE+OUTER;x++) {
      for (y=OUTER;y<SIZE+OUTER;y++) {
	if (canMove[x][y]==select) {
	  toX = x;
	  toY = y;
	  break;
	}
      }
      if (toX!=-1 || toY!=-1) break;
    }

    winPoint = playout(tP,toX,toY,nowX,nowY);
    
    winRate[select] = (winRate[select] * poCount[select] + winPoint) / (poCount[select] + 1);
    poCount[select] += 1;
  }

  long maxCount=0;
  select = -1;
  
  for (i=0;i<moveNum;i++) {
    if (maxCount<poCount[i]) {
      select = i;
      maxCount = poCount[i];
    }
  }

  switchMM = 0;
  if (select!=-1) return select;
  return 0;
}

int playout(int tP,int toX,int toY,int nowX,int nowY)
{
  Board sub_board;
  Fu sub_fu[KOMA+KOMA];
  int i,j,k,s;

  int canMove[SIZE+OUTER+OUTER][SIZE+OUTER+OUTER]={};
  int attackNum;
  
  for (s=0;s<KOMA+KOMA;s++) if (fu[s].place[0]==nowX && fu[s].place[1]==nowY) break;
  if (s==KOMA+KOMA) return 0;

  sub_board = board;
  for (i=0;i<KOMA+KOMA;i++) sub_fu[i] = fu[i];

  for (i=0;i<SIZE+OUTER+OUTER;i++) {
    for (j=0;j<SIZE+OUTER+OUTER;j++) {
      for (k=0;k<KOMA+KOMA;k++) {
	if (board.masu[fu[k].place[0]][fu[k].place[1]] == 0) exit(1);
      }
    }
  }
  
  board.masu[nowX][nowY] = 0;
  board.masu[toX][toY] = tP;
  fu[s].place[0] = toX;
  fu[s].place[1] = toY;
  fu[s].movable = 0;

  attackNum = createAttack(tP,toX,toY,canMove);

  //if (attackNum>0) doneAttack(canMove,1); // !!!!!!!!!!!!!!!!

  while (1) {
    //printf("~~~ in playout()\n");
    moveFu(1);
    
    if (board.komaNum[0]==0 || board.komaNum[1]==0) break;
    changeTurn(1);
  }
  //printf("~~~ out playout()\n");
  
  if (board.komaNum[tP-1]==0) k=0;
  else                        k=1;

  board = sub_board;
  for (i=0;i<KOMA+KOMA;i++) fu[i] = sub_fu[i];
  return k;
}


int entryToGame()
{  
  recv_Map = (char *)malloc(sizeof(recv_Map)*Recv_Size);
  send_Msg = (char *)malloc(sizeof(send_Msg)*(Send_Size+2));
  // IPv4 TCP のソケットを作成する
  if((sd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
    perror("socket");
    return -1;
  }
  
  // 送信先アドレスとポート番号を設定する
  addr.sin_family = AF_INET;
  addr.sin_port = htons(PORTNUM);
  addr.sin_addr.s_addr = inet_addr("127.0.0.1");
  //addr.sin_addr.s_addr = inet_addr("192.168.11.5"); 
  
  // サーバ接続（TCP の場合は、接続を確立する必要がある）
  connect(sd, (struct sockaddr *)&addr, sizeof(struct sockaddr_in));
  
  // パケットを TCP で受信
  if(recv(sd, recv_Map, Recv_Size, 0)<0){
    perror("recv");
    return -1;
  }
  printf("recv.DefaultMap\n%s\n",recv_Map);
  
  
  return 0;
}

void recvMapF()
{
  // パケットを TCP で受信
  if(recv(sd, recv_Map, Recv_Size, 0)<0){
    perror("recv");
    return;
  }
  printf("recv.Map\n%s\n",recv_Map);

  return;
}

void sendMsgF()
{
  printf("---- send is\n");
  printf("%s\n",send_Msg);
  // パケットを TCP で送信
  if(send(sd, send_Msg, Send_Size+2, 0) < 0) {
    perror("send");
    return;
  }
  printf("send end\n");

  return;
}

void createSend()
{
  int i,j,k;
  int numA,numB,numC;

  memset(send_Msg,'\0',strlen(send_Msg));

  for (k=0;k<moveID;k++) {
    //sprintf(send_Msg,"%s",send_Msg);
    numA = (move[k].oldX-OUTER)*SIZE+(move[k].oldY-OUTER);
    numB = (move[k].newX-OUTER)*SIZE+(move[k].newY-OUTER);
    numC = (move[k].attackX-OUTER)*SIZE+(move[k].attackY-OUTER);
    sprintf(send_Msg,"%s%d%d%d",send_Msg,numA/100,(numA%100)/10,numA%10);
    sprintf(send_Msg,"%s%d%d%d",send_Msg,numB/100,(numB%100)/10,numB%10);
    sprintf(send_Msg,"%s%d%d%d",send_Msg,numC/100,(numC%100)/10,numC%10);
  }
  sprintf(send_Msg,"%s999\n",send_Msg);
  
  return;
}

void changeFunction()
{

  // 無理やりつなぎなおし
  close(sd);
  printf("connect close\n");
  // IPv4 TCP のソケットを作成する
  if((sd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
    perror("socket");
    return;
  }
  // 送信先アドレスとポート番号を設定する
  addr.sin_family = AF_INET;
  addr.sin_port = htons(PORTNUM);
  addr.sin_addr.s_addr = inet_addr("127.0.0.1");
  connect(sd, (struct sockaddr *)&addr, sizeof(struct sockaddr_in));
  printf("connect reset\n");
  
  
}

void makeBoard()
{
  int x,y,i,k;

  board.komaNum[0] = 0;
  board.komaNum[1] = 0;
  if      (PORTNUM==20000) board.turnPlayer = 1;
  else if (PORTNUM==20001) board.turnPlayer = 2;
  i=0;

  for (x=0;x<SIZE+OUTER+OUTER;x++) {
    for (y=0;y<SIZE+OUTER+OUTER;y++) {
      if (x<OUTER || x>SIZE+OUTER-1 || y<OUTER || y>SIZE+OUTER-1) {
	board.masu[x][y] = -1;
      }
    }
  }

  k = -2;
  
  for (x=OUTER;x<SIZE+OUTER;x++) {
    for (y=OUTER;y<SIZE+OUTER;y++) {

      k += 2;
      
      if        (recv_Map[k]=='0') {
	board.masu[x][y] = 0;
	
      } else if (recv_Map[k]=='A') {
	board.masu[x][y] = 1;
	board.komaNum[0] += 1;
	fu[i].team = 1;
	fu[i].alive = 1;
	fu[i].health = recv_Map[k+1] - '0';
	if (fu[i].health==0) fu[i].alive = 0;
	fu[i].attack = 1;
	fu[i].walkDis = 3;
	if      (PORTNUM==20000) fu[i].movable = 1;
	else if (PORTNUM==20001) fu[i].movable = 0;
	fu[i].stayable = 1;
	fu[i].place[0] = x;
	fu[i].place[1] = y;
	
	i++;
	
      } else if (recv_Map[k]=='B') {
	board.masu[x][y] = 2;
	board.komaNum[1] += 1;
	fu[i].team = 2;
	fu[i].alive = 1;
	fu[i].health = recv_Map[k+1] - '0';
	if (fu[i].health==0) fu[i].alive = 0;
	fu[i].attack = 1;
	fu[i].walkDis = 3;
	if      (PORTNUM==20000) fu[i].movable = 0;
	else if (PORTNUM==20001) fu[i].movable = 1;
	fu[i].stayable = 1;
	fu[i].place[0] = x;
	fu[i].place[1] = y;
	
	i++;
	
      } else {
	printf("error: iniBoardN() -> initial char is unknown\n");
	continue;
      }
    }
  }

  for (;i<KOMA+KOMA;i++) {
    fu[i].alive = 0;
    fu[i].health = 0;
    fu[i].place[0] = 0;
    fu[i].place[1] = 0;
  }


  return;
}
